package hu.pte.ttk.snaptoshoot.util;

/*import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.RealDistribution;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.transform.FastFourierTransformer;*/
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.jtransforms.fft.DoubleFFT_1D;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import hu.pte.ttk.snaptoshoot.model.MathUtil;

public final class WaveUtil {

    public static byte[] readByteArray(final File rawFile)
            throws IOException {

        byte[] rawData = new byte[(int) rawFile.length()];
        InputStream input = null;
        try {
            input = new BufferedInputStream(new FileInputStream(rawFile));
            input.read(rawData);
        } finally {
            if (input != null) {
                input.close();
            }
        }

        return rawData;
    }

    public static void pcmToWav(final byte[] rawData, final File waveFile,
                                final int samplingRate, final short channel, final short encoding)
                                throws IOException {
        DataOutputStream output = null;
        try {
            output = new DataOutputStream(new FileOutputStream(waveFile));
            rawToWave(output, rawData, samplingRate, channel, encoding);
            output.write(rawData);
        } finally {
            if (output != null) {
                output.close();
            }
        }

    }

    // https://stackoverflow.com/questions/37281430/how-to-convert-pcm-file-to-wav-or-mp3
    public static void pcmToWav(final File rawFile, final File waveFile,
                                final int samplingRate, final short channel, final short encoding)
                                throws IOException {

        byte[] rawData = WaveUtil.readByteArray(rawFile);
        pcmToWav(rawData, waveFile, samplingRate, channel, encoding);
    }

    private static void rawToWave(DataOutputStream output, final byte[] rawData,
                                  final int samplingRate, final short channel, final short encoding)
                                  throws IOException {
        // WAVE header
        // see http://soundfile.sapp.org/doc/WaveFormat/
        writeString(output, "RIFF");                 // chunk id
        writeInt(output, 36 + rawData.length);       // chunk size = 4 + (8 + SubChunk1Size) + (8 + SubChunk2Size)
        writeString(output, "WAVE");                 // format
        writeString(output, "fmt ");                 // subchunk 1 id
        writeInt(output, 16);                        // subchunk 1 size
        writeShort(output, (short) 1);                     // audio format (1 = PCM)
        writeShort(output, channel);              // number of channels (mono = 1, stereo = 2)
        writeInt(output, samplingRate);                   // sample rate
        writeInt(output, samplingRate * channel * encoding / 8);// byte rate = SampleRate * NumChannels * BitsPerSample/8
        writeShort(output, (short)(channel * encoding / 8));           // block align = NumChannels * BitsPerSample/8
        writeShort(output, encoding);                    // bits per sample
        writeString(output, "data");                 // subchunk 2 id
        writeInt(output, rawData.length);                  // subchunk 2 size = NumSamples * NumChannels * BitsPerSample/8
        // Audio data (conversion big endian -> little endian)
        short[] shorts = new short[rawData.length / 2];
        ByteBuffer.wrap(rawData).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);
        ByteBuffer bytes = ByteBuffer.allocate(shorts.length * 2);
        for (short s : shorts) {
            bytes.putShort(s);
        }
    }

    public static byte[] removeWavHeader(byte[] wav){
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        os.write(wav, 44, wav.length - 44);
        return os.toByteArray();
    }

    public static double[] byteSignalToDouble(byte[] byteArray, final int sampleSize) {

        ByteBuffer byteBuffer = ByteBuffer.wrap(byteArray);
        double max = Math.pow(2, sampleSize -1);

        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);

        double[] samples = new double[byteArray.length * 8 / sampleSize];

        for (int i = 0; i < samples.length; i++) {
            switch(sampleSize) {
                case 8:  samples[i] = ( byteBuffer.get()      / max );
                    break;
                case 16: samples[i] = ( byteBuffer.getShort() / max );
                    break;
                case 32: samples[i] = ( byteBuffer.getInt()   / max );
                    break;
                case 64: samples[i] = ( byteBuffer.getLong()  / max );
                    break;
                default:
                    throw new UnsupportedOperationException("Sample size parameter is not valid!");
            }
        }

        return samples;
    }

    public static double[] fft1d(double[] data){

        DoubleFFT_1D fft_1D = new DoubleFFT_1D(data.length);
        fft_1D.realForward(data);

        return data;
    }

    public static double[] inverseFft1d(double[] data){

        DoubleFFT_1D fft_1D = new DoubleFFT_1D(data.length);
        fft_1D.realInverse(data, true);

        return data;
    }

    public static double[] crossCorrelation(double[] data, double[] data2, boolean normalize) throws Exception {

        if(!powerOfTwo(data.length) || !powerOfTwo(data2.length)){
            throw new Exception("Array length is not power of two!");
        }
        if(data.length != data2.length){
            throw new Exception("Length of the arrays are not equal!");
        }

        double[] res = new double[data.length];
        res[0] = data[0] * data2[0];
        res[1] = data[1] * data2[1];

        for (int i = 2; i < res.length; i += 2) {
            res[i] = data[i] * data2[i] - data[i+1] * data2[i+1];
            res[i+1] = data[i] * data2[i+1] + data[i+1] * data2[i];
        }

        return res;
    }

    private static double[][] normalize(double[] data1, double[] data2){

        int len = data1.length;
        double mean1 = MathUtil.avg(data1),
               mean2 = MathUtil.avg(data2);

        double stdDev1 = MathUtil.stdDev(data1),
               stdDev2 = MathUtil.stdDev(data2);

        double[] norm1 = new double[data1.length],
                 norm2 = new double[data2.length];

        for (int i=0; i<data1.length; i++){
            norm1[i] = (data1[i] - mean1) / (stdDev1 - len);
            norm2[i] = (data2[i] - mean2) / stdDev2;
        }

        return new double[][]{ norm1, norm2 };
    }

    public static double[] extendWithZeros(double[] data, int extendedLength){
        return Arrays.copyOf(data, extendedLength);
    }

    // bitwise check if power of two
    public static boolean powerOfTwo(int n)
    {
        return (n & n-1)==0;
    }

    public static void writeInt(final DataOutputStream output, final int value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
        output.write(value >> 16);
        output.write(value >> 24);
    }

    public static void writeShort(final DataOutputStream output, final short value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
    }

    public static void writeString(final DataOutputStream output, final String value) throws IOException {
        for (int i = 0; i < value.length(); i++) {
            output.write(value.charAt(i));
        }
    }
}

