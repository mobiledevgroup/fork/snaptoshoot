package hu.pte.ttk.snaptoshoot.model;

import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import androidx.annotation.NonNull;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import hu.pte.ttk.snaptoshoot.util.ByteUtil;

// AudioRecorder help:              https://stackoverflow.com/questions/8499042/android-audiorecord-example
// Convert pcm to wav & Audicity:   https://stackoverflow.com/questions/37281430/how-to-convert-pcm-file-to-wav-or-mp3
//                                  https://stackoverflow.com/questions/17192256/recording-wav-with-android-audiorecorder
//                                  https://developer.android.com/guide/topics/media/media-formats

public final class Recorder {

    // key have to contains how many channels: eg. mono: 1
    // value have to contains eg. AudioFormat.CHANNEL_IN_MONO(16)
    private final int CHANNEL;
    private final int ENCODING;
    private final int SAMPLING_RATE;
    private final int BufferElements2Rec;
    private final int BytesPerElement;

    public Recorder(final int RECORDER_CHANNELS, final int RECORDER_AUDIO_ENCODING, final int RECORDER_SAMPLING_RATE, final int BufferElements2Rec, final int BytesPerElement){
        this.CHANNEL = RECORDER_CHANNELS;
        this.ENCODING = RECORDER_AUDIO_ENCODING;
        this.SAMPLING_RATE = RECORDER_SAMPLING_RATE;
        this.BufferElements2Rec = BufferElements2Rec;
        this.BytesPerElement = BytesPerElement;
    }

    private AudioRecord recorder;
    private boolean isRecording = false;
    private Thread recordingThread = null;

    public boolean isRecording(){
        return (recorder != null) && recorder.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING;
    }

    public void startRecording(@NonNull final String filePath) throws IllegalStateException {
        this.initRecorder();

        isRecording = true;
        recorder.startRecording();

        recordingThread = new Thread(new Runnable() {
            public void run() {
                try {
                    final FileOutputStream os = new FileOutputStream(filePath);
                    storeRecording(os, new short[BufferElements2Rec]);
                } catch (FileNotFoundException e) {
                    Log.e("FileNotFound", filePath+" is not found!");
                    e.printStackTrace();
                } catch (IOException e){
                    Log.e("IO", "IOException with: "+filePath+"!");
                    e.printStackTrace();
                }
            }
        });
        recordingThread.start();

        Log.i("Recording","Recording is started");
    }

    private void storeRecording(final OutputStream os, short[] sData) throws IOException {
        // Write the output audio in byte
        // gets the voice output from microphone to byte format
        while (isRecording) {
            recorder.read(sData, 0, sData.length);

            // // writes the data to file from buffer
            // // stores the voice buffer
            os.write(ByteUtil.short2byte(sData));
        }
        os.close();
    }


    private void initRecorder(){
        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLING_RATE, CHANNEL, ENCODING,
                BytesPerElement*BufferElements2Rec
        );
    }

    public void stopRecording() throws IllegalStateException{
        if (recorder != null){
            isRecording = false;
            recorder.stop();
            recorder.release();
            recorder = null;
            recordingThread = null;
            Log.i("Recording","Recording stopped!");
        }
    }

}
