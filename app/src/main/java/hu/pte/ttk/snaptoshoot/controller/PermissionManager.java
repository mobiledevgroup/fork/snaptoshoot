package hu.pte.ttk.snaptoshoot.controller;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

//package private class - only MainActivity can instantiate it
final class PermissionManager {

    final static int REQUEST_PERMISSION_CODE = 1000;

    private final Activity activity;
    private final Context context;

    PermissionManager(@NonNull Activity a, @NonNull Context c){
        this.activity = a;
        this.context = c;
    }

    /**
     * @param perms - Multiple permission (Manifest.permission...)
     * @return - returns true if new permission needs to be requested, otherwise false
     */
    boolean requestPermissions(@NonNull final String ... perms){
        List<String> permList = new ArrayList<>();

        // Collecting not yet allowed permissions
        for (String perm : perms){
            if(!checkPermission(perm)){
                permList.add(perm);
            }
        }

        // If every permission is allowed return false, which means no need to request any permission
        if(permList.isEmpty()) return false;

        final String[] permArray = new String[permList.size()];
        permList.toArray(permArray);

        this.requestPermission(permArray);

        return true;
    }

    void requestPermission(String ... permArray){
        ActivityCompat.requestPermissions(this.activity,
                permArray,
                REQUEST_PERMISSION_CODE);
    }


    boolean checkPermission(String perm){
        return ContextCompat.checkSelfPermission(this.context, perm) == PackageManager.PERMISSION_GRANTED;
    }

}
