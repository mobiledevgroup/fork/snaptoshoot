package hu.pte.ttk.snaptoshoot.model;

public class Gauss {

    public double x0;
    public double sigma;
    public double P;
    public double A;

    public Gauss(double x0, double sigma, double P, double A){
        this.x0 = x0;
        this.sigma = sigma;
        this.P = P;
        this.A = A;
    }

    public static void gaussianize(int length, Gauss o){
        int f = -(length/2);
        int t = (length+1)/2;

        o.x0 = 0.5 * o.x0 * length;
        o.sigma = 0.5 * o.sigma * length;
        int out = 0;
        for(int i=f; i<t; i++){
            out *= gauss_at(i, o);
        }
    }

    public static double gauss_at(double x, double x0, double sigma, double P, double A){
        return A * java.lang.Math.exp(Math.pow(((x-x0) * (x-x0) / (sigma*sigma)),P));
    }
    public static double gauss_at(double x, Gauss o) {
        return o.A * java.lang.Math.exp(Math.pow(((x - o.x0) * (x - o.x0) / (o.sigma * o.sigma)), o.P));
    }
}


