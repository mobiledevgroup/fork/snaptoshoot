package hu.pte.ttk.snaptoshoot.model;

public class MathUtil {

    public static double stdDev(double[] data, double mean){
        double standardDeviation = 0.0;
        for(double d: data) {
            standardDeviation += Math.pow(d - mean, 2);
        }

        return Math.sqrt(standardDeviation/data.length);
    }

    public static double stdDev(double[] data){
        return stdDev(data, avg(data));
    }

    public static double avg(double[] data){
        double sum = 0.0;
        for (double d: data) {
            sum += d;
        }
        return sum / data.length;
    }

}
